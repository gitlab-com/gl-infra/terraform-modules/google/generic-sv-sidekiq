resource "google_compute_disk" "urgent_cpu_bound_log_disk" {
  project = var.project
  count   = var.sidekiq_urgent_cpu_bound_count
  name = format(
    "%v-urgent-cpu-bound-%02d-%v-%v-log",
    var.name,
    count.index + 1,
    var.tier,
    var.environment,
  )
  zone = var.zone != "" ? var.zone : element(data.google_compute_zones.available.names, count.index + 1)
  size = var.log_disk_size
  type = var.log_disk_type

  labels = {
    environment = var.environment
  }
}

resource "google_compute_instance" "sidekiq_urgent_cpu_bound" {
  allow_stopping_for_update = var.allow_stopping_for_update
  count                     = var.sidekiq_urgent_cpu_bound_count
  name = format(
    "%v-urgent-cpu-bound-%02d-%v-%v",
    var.name,
    count.index + 1,
    var.tier,
    var.environment,
  )
  machine_type = var.sidekiq_urgent_cpu_bound_instance_type

  metadata = {
    "CHEF_URL"     = var.chef_provision["server_url"]
    "CHEF_VERSION" = var.chef_provision["version"]
    "CHEF_NODE_NAME" = var.use_new_node_name ? format(
      "%v-urgent-cpu-bound-%02d-%v-%v.c.%v.internal",
      var.name,
      count.index + 1,
      var.tier,
      var.environment,
      var.project,
      ) : format(
      "%v-urgent-cpu-bound-%02d.%v.%v.%v",
      var.name,
      count.index + 1,
      var.tier,
      var.environment,
      var.dns_zone_name,
    )
    "GL_KERNEL_VERSION"      = var.kernel_version
    "CHEF_ENVIRONMENT"       = var.environment
    "CHEF_RUN_LIST"          = "\"role[${var.environment}-base-be-sidekiq-urgent-cpu-bound]\""
    "CHEF_DNS_ZONE_NAME"     = var.dns_zone_name
    "CHEF_PROJECT"           = var.project
    "CHEF_BOOTSTRAP_BUCKET"  = var.chef_provision["bootstrap_bucket"]
    "CHEF_BOOTSTRAP_KEYRING" = var.chef_provision["bootstrap_keyring"]
    "CHEF_BOOTSTRAP_KEY"     = var.chef_provision["bootstrap_key"]
    "block-project-ssh-keys" = var.block_project_ssh_keys
    "enable-oslogin"         = var.enable_oslogin
    "shutdown-script"        = module.bootstrap.teardown
    "startup-script"         = module.bootstrap.bootstrap
  }

  project = var.project
  zone    = var.zone != "" ? var.zone : element(data.google_compute_zones.available.names, count.index + 1)

  service_account {
    // this should be the instance under which the instance should be running, rather than the one creating it...
    email = var.service_account_email

    // all the defaults plus cloudkms to access kms
    scopes = [
      "https://www.googleapis.com/auth/cloud.useraccounts.readonly",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring.write",
      "https://www.googleapis.com/auth/pubsub",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/trace.append",
      "https://www.googleapis.com/auth/cloudkms",
      "https://www.googleapis.com/auth/compute.readonly",
    ]
  }

  scheduling {
    preemptible = var.preemptible
  }

  boot_disk {
    auto_delete = true

    initialize_params {
      image = var.os_boot_image
      size  = var.sidekiq_urgent_cpu_bound_os_disk_size != "" ? var.sidekiq_urgent_cpu_bound_os_disk_size : var.os_disk_size
      type  = var.os_disk_type
    }
  }

  attached_disk {
    source      = google_compute_disk.urgent_cpu_bound_log_disk[count.index].self_link
    device_name = "log"
  }

  network_interface {
    subnetwork = google_compute_subnetwork.sidekiq.name
    dynamic "access_config" {
      for_each = var.assign_public_ip ? [0] : []
      content {
      }
    }
  }

  labels = {
    environment = var.environment
    pet-name    = "${var.name}-urgent-cpu-bound"
  }

  tags = [
    var.name,
    "${var.name}-urgent-cpu-bound",
    var.environment,
  ]

  lifecycle {
    ignore_changes = [min_cpu_platform, boot_disk, machine_type]
  }
}

output "sidekiq_urgent_cpu_bound_self_link" {
  value = google_compute_instance.sidekiq_urgent_cpu_bound.*.self_link
}

